#include "simulator_gazebo_plugin.hpp"

#include <math.h>
#include <tf2_ros/transform_broadcaster.h>

#include <iostream>

#include "terminal.h"
#include <gazebo/physics/physics.hh>
#include <gazebo_ros/node.hpp>

#include <geometry_msgs/msg/transform_stamped.hpp>

namespace gokart_gazebo_plugin
{
GokartGazeboPlugin::GokartGazeboPlugin()
  : last_sim_time_{ 0 }, last_update_time_{ 0 }, desired_steering_angle_{ 0.0 }, desired_velocity_{ 0.0 }
{
}

void GokartGazeboPlugin::LoadParameters(sdf::ElementPtr sdf)
{
  map_frame_name_ = sdf->GetElement("mapFrameName")->Get<std::string>();
  base_link_name_ = sdf->GetElement("baseLinkName")->Get<std::string>();
  fl_steering_joint_name_ = sdf->GetElement("frontLeftSteeringJointName")->Get<std::string>();
  fr_steering_joint_name_ = sdf->GetElement("frontRightSteeringJointName")->Get<std::string>();
  fl_motor_joint_name_ = sdf->GetElement("frontLeftMotorJointName")->Get<std::string>();
  fr_motor_joint_name_ = sdf->GetElement("frontRightMotorJointName")->Get<std::string>();
  rl_motor_joint_name_ = sdf->GetElement("rearLeftMotorJointName")->Get<std::string>();
  rr_motor_joint_name_ = sdf->GetElement("rearRightMotorJointName")->Get<std::string>();
  publish_ground_truth_transform_ = sdf->GetElement("publishGroundTruthTransform")->Get<bool>();
  publish_joint_states_ = sdf->GetElement("publishJointStates")->Get<bool>();
}

void GokartGazeboPlugin::Load(gazebo::physics::ModelPtr model, sdf::ElementPtr sdf)
{
  // save model reference
  model_ = model;

  // TODO: consider just checking that correct friction_model is set
  //       and fail with a log message when it is not
  world_ = model_->GetWorld();
  auto physicsEngine = world_->Physics();
  physicsEngine->SetParam("friction_model", std::string{ "cone_model" });

  LoadParameters(sdf);

  // save model's base_link reference
  fprintf(stderr, "base_link_name_ = %s\n", base_link_name_.c_str());
  base_link_ = model_->GetLink(base_link_name_);

  // set up ROS node, subscribers and publishers

  ros_node_ = gazebo_ros::Node::Get(sdf);

  RCLCPP_INFO(ros_node_->get_logger(), red("Setting up ROS node..."));

  ground_truth_pub_ = ros_node_->create_publisher<Odometry>("/ground_truth", 1);
  odometry_pub_ = ros_node_->create_publisher<Odometry>("/odometry", 1);

  joint_state_pub_ = ros_node_->create_publisher<JointState>("/joint_states", 1);

  tf_broadcaster_ = std::make_unique<tf2_ros::TransformBroadcaster>(*ros_node_);

  control_command_sub_ =
      ros_node_->create_subscription<AckControlCommand>("/control_cmd", 1, [=](AckControlCommand::SharedPtr msg) {
        // RCLCPP_INFO(ros_node_->get_logger(), red("Receiving new command message"));

        // 0.6 rad ~ 34 deg

        if (msg->drive.steering_angle < -max_steering_angle_)
        {
          desired_steering_angle_ = -max_steering_angle_;
        }
        else if (msg->drive.steering_angle > max_steering_angle_)
        {
          desired_steering_angle_ = max_steering_angle_;
        }
        else
        {
          desired_steering_angle_ = msg->drive.steering_angle;
        }

        // 20 mps = 72 kmph = 44.74 mph
        if (msg->drive.speed < max_velocity_backward_)
        {
          desired_velocity_ = max_velocity_backward_;
        }
        else if (msg->drive.speed > max_velocity_forward_)
        {
          desired_velocity_ = max_velocity_forward_;
        }
        else
        {
          desired_velocity_ = msg->drive.speed;
        }
      });

  front_left_steering.SetJoint(fl_steering_joint_name_, 2.7, 0.5, 0.3);
  front_left_steering.joint_ = model_->GetJoint(fl_steering_joint_name_);

  front_right_steering.SetJoint(fr_steering_joint_name_, 2.7, 0.5, 0.3);
  front_right_steering.joint_ = model_->GetJoint(fr_steering_joint_name_);

  front_left_motor.SetJoint(fl_motor_joint_name_, 0.0, 0.0, 0.0);
  front_left_motor.joint_ = model_->GetJoint(fl_motor_joint_name_);

  front_right_motor.SetJoint(rr_motor_joint_name_, 0.0, 0.0, 0.0);
  front_right_motor.joint_ = model_->GetJoint(fr_motor_joint_name_);

  rear_left_motor.SetJoint(rl_motor_joint_name_, 4.8, 2.8, 0.0);
  rear_left_motor.joint_ = model_->GetJoint(rl_motor_joint_name_);

  rear_right_motor.SetJoint(rr_motor_joint_name_, 4.8, 2.8, 0.0);
  rear_right_motor.joint_ = model_->GetJoint(rr_motor_joint_name_);

  auto track = world_->ModelByName(std::string{ "track" });  // load the cones
  if (track)
  {
    std::cout << "FOUND THE TRACK " << std::endl;
    cones_ = track->GetLinks();
  }
  rel_cones_pub_ = ros_node_->create_publisher<ConeData>("cones_points", 10);
  rel_cones_cloud_pub_ = ros_node_->create_publisher<sensor_msgs::msg::PointCloud2>("cones_pointcloud", 10);

  // Hook into simulation update loop
  update_connection_ = gazebo::event::Events::ConnectWorldUpdateBegin(std::bind(&GokartGazeboPlugin::Update, this));
}

void GokartGazeboPlugin::Update()
{
  auto cur_time = world_->SimTime();

  if (last_sim_time_ == 0)
  {
    last_sim_time_ = cur_time;
    last_update_time_ = cur_time;
    return;
  }
  if (first_ && base_link_->WorldPose() != ignition::math::Pose3d())
  {
    init_pose_ = base_link_->WorldPose();
    corrected_init_pose_ = init_pose_;
    ignition::math::Quaterniond correction_purdue;
    correction_purdue.EulerToQuaternion(0, 0, .57);
    corrected_init_pose_.Rot() *= correction_purdue;  // rotate by the correction
    double current_y = corrected_init_pose_.Y();
    corrected_init_pose_.SetY(current_y + 2.5);
    std::cout << "INIT POSE " << init_pose_ << std::endl;
    first_ = false;
  }

  // ground_truth_pub
  ground_truth_msg_.header.stamp.sec = cur_time.sec;
  ground_truth_msg_.header.stamp.nanosec = cur_time.nsec;
  ground_truth_msg_.header.frame_id = "odom";
  ground_truth_msg_.child_frame_id = base_link_name_;
  ignition::math::Pose3d pose = base_link_->WorldPose() - init_pose_;  // WorldLinearVel
  ground_truth_msg_.pose.pose.position.x = pose.Pos().X();
  ground_truth_msg_.pose.pose.position.y = pose.Pos().Y();
  ground_truth_msg_.pose.pose.position.z = pose.Pos().Z();
  ground_truth_msg_.pose.pose.orientation.x = pose.Rot().X();
  ground_truth_msg_.pose.pose.orientation.y = pose.Rot().Y();
  ground_truth_msg_.pose.pose.orientation.z = pose.Rot().Z();
  ground_truth_msg_.pose.pose.orientation.w = pose.Rot().W();
  ignition::math::Vector3d lin_velocity = base_link_->RelativeLinearVel();
  ground_truth_msg_.twist.twist.linear.x = lin_velocity.X();
  ground_truth_msg_.twist.twist.linear.y = lin_velocity.Y();
  ground_truth_msg_.twist.twist.linear.z = lin_velocity.Z();
  ignition::math::Vector3d angular_velocity = base_link_->RelativeAngularVel();
  ground_truth_msg_.twist.twist.angular.x = angular_velocity.X();
  ground_truth_msg_.twist.twist.angular.y = angular_velocity.Y();
  ground_truth_msg_.twist.twist.angular.z = angular_velocity.Z();
  ignition::math::Vector3d lin_accel = base_link_->RelativeLinearAccel();
  ground_truth_msg_.accel.linear.x = lin_accel.X();
  ground_truth_msg_.accel.linear.y = lin_accel.Y();
  ground_truth_msg_.accel.linear.z = lin_accel.Z();
  ignition::math::Vector3d ang_accel = base_link_->RelativeAngularAccel();
  ground_truth_msg_.accel.angular.x = ang_accel.X();
  ground_truth_msg_.accel.angular.y = ang_accel.Y();
  ground_truth_msg_.accel.angular.z = ang_accel.Z();
  if (publish_ground_truth_transform_)
  {
    // tf publisher

    ground_truth_tf_pub_.header.stamp.sec = cur_time.sec;
    ground_truth_tf_pub_.header.stamp.nanosec = cur_time.nsec;
    ground_truth_tf_pub_.header.frame_id = map_frame_name_;
    ground_truth_tf_pub_.child_frame_id = "odom";
    ground_truth_tf_pub_.transform.translation.x = init_pose_.Pos().X();
    ground_truth_tf_pub_.transform.translation.y = init_pose_.Pos().Y();
    ground_truth_tf_pub_.transform.translation.z = init_pose_.Pos().Z();

    ground_truth_tf_pub_.transform.rotation.x = init_pose_.Rot().X();
    ground_truth_tf_pub_.transform.rotation.y = init_pose_.Rot().Y();
    ground_truth_tf_pub_.transform.rotation.z = init_pose_.Rot().Z();
    ground_truth_tf_pub_.transform.rotation.w = init_pose_.Rot().W();

    geometry_msgs::msg::TransformStamped odometry_tf;
    odometry_tf.header.stamp.sec = cur_time.sec;
    odometry_tf.header.stamp.nanosec = cur_time.nsec;
    odometry_tf.header.frame_id = "odom";
    odometry_tf.child_frame_id = base_link_name_;
    odometry_tf.transform.translation.x = pose.Pos().X();
    odometry_tf.transform.translation.y = pose.Pos().Y();
    odometry_tf.transform.translation.z = pose.Pos().Z();
    odometry_tf.transform.rotation.x = pose.Rot().X();
    odometry_tf.transform.rotation.y = pose.Rot().Y();
    odometry_tf.transform.rotation.z = pose.Rot().Z();
    odometry_tf.transform.rotation.w = pose.Rot().W();

    // Send the transformation
    tf_broadcaster_->sendTransform(odometry_tf);
    tf_broadcaster_->sendTransform(ground_truth_tf_pub_);
  }

  if (publish_joint_states_)
  {
    // joint state publisher
    joint_state_msg_.header.stamp.sec = cur_time.sec;
    joint_state_msg_.header.stamp.nanosec = cur_time.nsec;
    joint_state_msg_.header.frame_id = base_link_name_;
    joint_state_msg_.name = { rl_motor_joint_name_,    rr_motor_joint_name_, fl_steering_joint_name_,
                              fr_steering_joint_name_, fl_motor_joint_name_, fr_motor_joint_name_ };
    joint_state_msg_.position = { rear_left_motor.joint_->Position(0),     rear_right_motor.joint_->Position(0),
                                  front_left_steering.joint_->Position(0), front_right_steering.joint_->Position(0),
                                  front_left_motor.joint_->Position(0),    front_right_motor.joint_->Position(0) };
    joint_state_msg_.velocity = {
      rear_left_motor.joint_->GetVelocity(0),     rear_right_motor.joint_->GetVelocity(0),
      front_left_steering.joint_->GetVelocity(0), front_right_steering.joint_->GetVelocity(0),
      front_left_motor.joint_->GetVelocity(0),    front_right_motor.joint_->GetVelocity(0)
    };
    joint_state_msg_.effort = { rear_left_motor.joint_->GetForce(0),     rear_right_motor.joint_->GetForce(0),
                                front_left_steering.joint_->GetForce(0), front_right_steering.joint_->GetForce(0),
                                front_left_motor.joint_->GetForce(0),    front_right_motor.joint_->GetForce(0) };

    joint_state_pub_->publish(joint_state_msg_);
  }

  auto dt = (cur_time - last_sim_time_).Double();

  // Compute PID for speed

  double wheel_radius = 0.14;  // [m] TODO compute automaticaly from joint definition

  double desired_radial_velocity = desired_velocity_ / wheel_radius;

  // need to check if id of rotation axis is 0
  auto err_rear_left = rear_left_motor.joint_->GetVelocity(0) - desired_radial_velocity;
  auto err_rear_right = rear_right_motor.joint_->GetVelocity(0) - desired_radial_velocity;

  auto force_rear_left = rear_left_motor.pid.Update(err_rear_left, dt);
  auto force_rear_right = rear_right_motor.pid.Update(err_rear_right, dt);

  // need to chceck if id of rotation axis is 0
  rear_left_motor.joint_->SetForce(0, force_rear_left);
  rear_right_motor.joint_->SetForce(0, force_rear_right);

  // car dimensions
  double L = 1.050;  // wheelbase [m] TODO compute automaticaly from joint definition
  double T = 1.1;    // track width [m] TODO compute automaticaly from joint definition

  // compute ackermann geometry
  double front_left_steering_desired_angle;
  double front_right_steering_desired_angle;

  if (desired_steering_angle_ < 0.005 && desired_steering_angle_ > -0.005)
  {
    front_left_steering_desired_angle = desired_steering_angle_;
    front_right_steering_desired_angle = desired_steering_angle_;
  }
  else
  {
    front_left_steering_desired_angle = atan(L / ((L / tan(desired_steering_angle_)) - T / 2.0));
    front_right_steering_desired_angle = atan(L / ((L / tan(desired_steering_angle_)) + T / 2.0));
  }

  // compute PID for steering
  // need to chceck if id of rotation axis is 0
  auto err_front_left_steer = front_left_steering.joint_->Position(0) - front_left_steering_desired_angle;
  auto err_front_right_steer = front_right_steering.joint_->Position(0) - front_right_steering_desired_angle;

  auto force_front_left_steer = front_left_steering.pid.Update(err_front_left_steer, dt);
  auto force_front_right_steer = front_right_steering.pid.Update(err_front_right_steer, dt);

  // need to chceck if id of rotation axis is 0
  front_left_steering.joint_->SetForce(0, force_front_left_steer);
  front_right_steering.joint_->SetForce(0, force_front_right_steer);

  // RCLCPP_INFO(
  //   ros_node_->get_logger(),
  //   CSI_RED + std::to_string(cur_time.sec) + "  " +
  //     std::to_string(rear_left_motor.joint_->GetVelocity(0)) + "  " +
  //     std::to_string(rear_right_motor.joint_->GetVelocity(0)) + rst);
  ConeData cone_data;
  pcl::PointCloud<pcl::PointXYZ> cone_cloud;
  for (const auto& cone : cones_)
  {
    auto dist = cone->WorldPose().Pos().Distance(model_->WorldPose().Pos());
    if (dist < 20.0)
    {
      auto rel_cone = cone->WorldPose() - model_->WorldPose();
      geometry_msgs::msg::Point32 rel_cone_p;
      rel_cone_p.x = rel_cone.X();
      rel_cone_p.y = rel_cone.Y();
      rel_cone_p.z = rel_cone.Z();
      cone_data.rel_position.push_back(rel_cone_p);
      cone_data.ids.push_back(cone->GetId());
      cone_cloud.push_back(pcl::PointXYZ(rel_cone.X(), rel_cone.Y(), rel_cone.Z()));
    }
  }
  if ((cur_time - last_update_time_).Double() >= .04)
  {
    sensor_msgs::msg::PointCloud2 point_cloud_msg;
    pcl::toROSMsg(cone_cloud, point_cloud_msg);
    point_cloud_msg.header.stamp.sec = cur_time.sec;
    point_cloud_msg.header.stamp.nanosec = cur_time.nsec;
    point_cloud_msg.header.frame_id = "base_link";  // TODO make this configurable
    rel_cones_cloud_pub_->publish(point_cloud_msg);

    cone_data.header.stamp.sec = cur_time.sec;
    cone_data.header.stamp.nanosec = cur_time.nsec;
    cone_data.header.frame_id = "base_link";  // TODO make this configurable
    rel_cones_pub_->publish(cone_data);
    ground_truth_pub_->publish(ground_truth_msg_);
    last_update_time_ = cur_time;
  }
  last_sim_time_ = cur_time;
}

GZ_REGISTER_MODEL_PLUGIN(GokartGazeboPlugin)

}  // namespace gokart_gazebo_plugin
