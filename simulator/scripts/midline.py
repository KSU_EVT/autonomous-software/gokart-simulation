#!/usr/bin/env python3
import math
import csv
import os

def center_line_from_csv(lpath, rpath):
    lpoints = list_from_csv(lpath)
    rpoints = list_from_csv(rpath)
    if lpoints is None or rpoints is None:
        return None
    else:
        return create_center_line(lpoints, rpoints)

def list_from_csv(path):
    path = os.path.expanduser(path)
    path = os.path.expandvars(path)

    csv_file = open(path)
    reader = csv.reader(csv_file)
    raw_list = list(reader)
    if not len(raw_list) > 1:
        return None
    if not (raw_list[0][0] == 'ID' and raw_list[0][1] == 'x' and raw_list[0][2] == 'y'):
        return None
    if not len(raw_list[0]) <= 3:
        return None

    # remove 0th index (data labels)
    raw_list = raw_list[1:]
    outlist = []

    for aRecord in raw_list:
        outlist.append((float(aRecord[1]), float(aRecord[2])))

    return(outlist)


def create_center_line(lpoints, rpoints):
    if lpoints is None or rpoints is None:
        return None
    if len(lpoints) == 0 or len(rpoints) == 0:
        return None

    # Make sure lpoints is shorter than rpoints
    if len(lpoints) > len(rpoints):
        temp = lpoints
        lpoints = rpoints
        rpoints = temp

    mpoints = []
    for L in lpoints:
        minD = dist(L, rpoints[0])
        minR = rpoints[0]
        midpoint = None
        for R in rpoints:
            d = dist(L,R)
            if d <= minD:
                minD = d
                minR = R
                if len(lpoints[0]) <= 2:
                    midpoint = (L[0] + minR[0])/2, (L[1] + minR[1])/2 # x, y
                else:
                    midpoint = (L[0] + minR[0])/2, (L[1] + minR[1])/2, (L[2] + minR[2])/2 # x, y, z
        mpoints.append(midpoint)

    return mpoints

def dist(a, b):
    if a is None or b is None:
        return None

    if len(a) <= 2:
        x1, y1 = a
        x2, y2 = b
        d = math.sqrt(float((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1))) #2D dist
    else:
        x1, y1, z1 = a
        x2, y2, z2 = a
        d = math.sqrt(float((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1))) #3D dist
    return d

def mean_center_dist(points):
    if len(points) == 0:
        return None
    if len(points) == 1:
        return 0

    center = []
    for aDimension in points[0]:
        center.append(float(0))
    # Sum
    for aPoint in points:
        for i in range(len(aPoint)):
            center[i] += aPoint[i]
    # Average
    for i in range(len(center)):
        center[i] = center[i] / len(points)

    mean_dist = 0

    # Sum
    for aPoint in points:
        mean_dist += dist(center, aPoint)
    # Average
    mean_dist = mean_dist / len(points)
    return(mean_dist)

if __name__ == "__main__":
    print(center_line_from_csv("./cones_left-20220402-0107.csv", "./cones_right-20220402-0107.csv"))
