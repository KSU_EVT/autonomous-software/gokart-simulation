#!/usr/bin/env python3
import math
import os

import rclpy
import numpy as np
from rclpy.node import Node
from gen_racetrack import load_wall, get_earth_radius_at_latitude, convert_points, trajectory_interpolate
from rcl_interfaces.msg import ParameterDescriptor, ParameterType, FloatingPointRange, Parameter, SetParametersResult
from typing import List, Tuple
from ament_index_python import get_package_share_directory

from geometry_msgs.msg import Point32
from geometry_msgs.msg import PolygonStamped

import midline

class CenterlineCreator(Node):

    def __init__(self):
        super().__init__('centerline_creator')

        self.declare_parameter(
            name='interpolation_distance',
            descriptor=ParameterDescriptor(
                type=ParameterType.PARAMETER_DOUBLE,
                floating_point_range=[FloatingPointRange(
                    from_value=0.00,
                    to_value=float(100),
                    step=0.0,
                )],
                description='distance between each point on the centerline spline',
            ),
            value=1.0,
        )

        self.declare_parameter(
            name='gnss_data_paths',
            descriptor=ParameterDescriptor(
                type=ParameterType.PARAMETER_STRING_ARRAY,
                description='absolute paths to gnss .csv data',
            ),
            value=[
                get_package_share_directory('simulator')
                + '/models/purdue_racetrack/gps_data/left.csv',
                get_package_share_directory('simulator')
                + '/models/purdue_racetrack/gps_data/right.csv',
            ],
        )

        self.declare_parameter(
            name='gnss_origin_point',
            descriptor=ParameterDescriptor(
                type=ParameterType.PARAMETER_DOUBLE_ARRAY,
                description='array of three numbers [longitude, latitude, elevation], this point will be '
                            'transformed to the origin [0,0,0] in the cartesian coordinate system in Gazebo simulation',
            ),
            value=[-86.945105, 40.437265, 0.0],
        )
        self.add_on_set_parameters_callback(self.reconfigure_callback)
        # init spawn request
        # parameters (automatically updated via self.update_parameters())
        self.gnss_data_paths = []
        self.interpolation_distance = 1.0
        self.gnss_origin_point = [0, 0, 0]



        self.update_parameters([
            self.get_parameter('gnss_data_paths'),
            self.get_parameter('interpolation_distance'),
            self.get_parameter('gnss_origin_point'),
        ])


        self.centerline_points = []
        self.publisher = self.create_publisher(PolygonStamped, 'centerline', 10)

        self.create_centerline()

        self.timer = self.create_timer(0.5, self.timer_callback)

    def timer_callback(self):
        print('got here')
        msg = PolygonStamped()
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.polygon.points = self.centerline_points
        msg.header.frame_id = "map"
        self.publisher.publish(msg)


    def update_parameters(self, parameters: List[Parameter]) -> None:
        # validate parameters
        for param in parameters:
            if param.name == 'gnss_data_paths':
                self.gnss_data_paths = param.value
            elif param.name == 'interpolation_distance':
                self.interpolation_distance = param.value
            elif param.name == 'gnss_origin_point':
                self.gnss_origin_point = param.value

    def create_centerline(self):
        self.get_logger().info(
            f'spawning centerline with dist = {self.interpolation_distance}'
        )

        self.radius_north_, self.radius_east_ = get_earth_radius_at_latitude(
            latitude=self.gnss_origin_point[1],
        )

        all_cones = []

        point_list_list = []
        for path in self.gnss_data_paths:
            loaded_gnss_points = load_wall(path)

            points_cartesian = convert_points(
                points_gps=loaded_gnss_points,
                base_point_gps=self.gnss_origin_point,
                radius_north=self.radius_north_,
                radius_east=self.radius_east_,
                num_points=None,
                visualize=False,
            )

            # # DEBUG
            # print(f"points_cartesian[0] is: {points_cartesian[0]}")
            # print(f"points_cartesian[1] is: {points_cartesian[1]}")
            # # point_list = midline.list_from_csv(path)

            xy_points = []
            for np_point in points_cartesian:
                xy_points.append((np_point[0], np_point[1]))

            mean_dist = midline.mean_center_dist(xy_points)
            # used as a priority stack
            point_list_list.append((mean_dist, xy_points))

            cones = self.get_cones_poses(
                self.interpolation_distance,
                points_cartesian
            )


            # set_name = path.split(sep='/')[-1].split(sep='.')[0]
            set_name = os.path.split(path)[1].split(sep='.')[0]

            all_cones.append((set_name, cones))

        point_list_list.sort() # smallest to largest mean_dist
        rpoints = point_list_list.pop()[1] # largest mean_dist
        lpoints = point_list_list.pop()[1] # second largest mean_dist
        midline_points = midline.create_center_line(lpoints, rpoints) # pairwise order here doesn't matter

        self.centerline_points = [Point32(x=p[0], y=p[1]) for p in midline_points]


    def get_cones_poses(self, interpolation_distance: float, border_points: np.array) -> List[Tuple[Point32, float]]:
        border_length = np.sum(np.sqrt(
            np.sum(np.diff(border_points, axis=0) ** 2, axis=1)
        ))

        number_of_cones = round(border_length / interpolation_distance)

        cones_position = trajectory_interpolate(
            border_points, int_size=number_of_cones
        )

        cones = []

        for i in range(len(cones_position)):
            cone_position = Point32()
            cone_position.x = cones_position[i][0]
            cone_position.y = cones_position[i][1]
            cone_position.z = 0.0

            yaw = math.atan2(
                # y
                cones_position[(i + 1) % len(cones_position)][1]
                - cones_position[i - 1][1],
                # x
                cones_position[(i + 1) % len(cones_position)][0]
                - cones_position[i - 1][0]
            )

            cones.append((cone_position, yaw))

        return cones

    def reconfigure_callback(self, parameters: List[Parameter]) -> SetParametersResult:
        """Called when there is a request to set one or multiple parameters
        Called even for the initial parameter declaration (if registered before the declaration).
        Registered using self.add_on_set_parameters_callback(self.reconfigure_callback).
        Called for each parameter separately (i.e. len(parameters) == 1),
        unless multiple parameters are set using set_parameters_atomically (then len(parameters) >= 1).
        Before this callback is called, parameters' values are validated against their specified constraints (if any).
        If type or constraints validation fails, this callback will not be called at all.
        If this callback returns SetParametersResult(successful=False), the values will not be set.
        """

        self.update_parameters(parameters)
        self.create_centerline()

        return SetParametersResult(successful=True)


def main(args=None):
    rclpy.init(args=args)
    centerline_creator = CenterlineCreator()
    rclpy.spin(centerline_creator)
    rclpy.shutdown()


if __name__ == '__main__':
    main()
